import Vue from 'vue'
import App from './App.vue'
import router from './router'
import axios from 'axios'
import 'bootstrap' // Import js file
import 'bootstrap/dist/css/bootstrap.min.css' // Import css file
import VueAxios from 'vue-axios'
import store from './store'
import ElementUI from 'element-ui'

Vue.use(ElementUI)
Vue.config.productionTip = false
Vue.prototype.$axios = axios
Vue.prototype.$VueAxios = VueAxios

// 登入 token 驗證
router.beforeEach((to, from, next) => {
  const isLogin = store.state.token === localStorage.getItem('token')
  if (isLogin) {
    // console.log(localStorage.getItem('token'))
    // console.log('store:', store.state.token)
    next()
  } else {
    if (to.path !== '/Login') {
      // alert('請先登入')
      // next('/Login')
      next()
    } else { next() }
  }
  next()
})

// 刷新頁面 store 重新取得 token
if (localStorage.getItem('token')) {
  store.commit('set_token', localStorage.getItem('token'))
  store.commit('set_name', localStorage.getItem('userName'))
}
if (localStorage.getItem('set_apiUrl')) {
  store.commit('set_apiUrl', localStorage.getItem('set_apiUrl'))
}
if (localStorage.getItem('set_site_code')) {
  store.commit('set_site_code', localStorage.getItem('set_site_code'))
}
if (localStorage.getItem('set_user_level_id')) {
  store.commit('set_user_level_id', localStorage.getItem('set_user_level_id'))
}

export const eventBus = new Vue()
new Vue({
  el: '#app',
  components: { App },
  template: '<App/>',
  render: h => h(App),
  router,
  store,
})

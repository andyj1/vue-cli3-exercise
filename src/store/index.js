import Vue from 'vue'
import vuex from 'vuex'
Vue.use(vuex)

export default new vuex.Store({
  state: {
    token: '',
    userName: '',
    errorCode: '',
    apiUrl: '',
    site_code: '',
    user_level_id: '',
  },
  mutations: {
    set_token (state, toke) {
      state.token = toke
    },
    set_name (state, userName) {
      state.userName = userName
    },
    set_errorCode (state, errorCode) {
      state.errorCode = errorCode
    },
    set_apiUrl (state, apiUrl) {
      state.apiUrl = apiUrl
    },
    set_site_code (state, site_code) {
      state.site_code = site_code
    },
    set_user_level_id (state, user_level_id) {
      state.user_level_id = user_level_id
    },
  },
})

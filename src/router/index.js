import Vue from 'vue'
import VueRouter from 'vue-router' // 載入 vue-router
import Home from '@/components/Home'
import Login from '@/components/Login'
import tinymceEditor from '@/components/tinymceEditor'
import elementUI from '@/components/elementUI'
import MemberManagement from '@/components/Member_management'
import MemberLevels from '@/components/Member_levels'
import levelUserList from '@/components/level-user-list'
// import Button from '@/components/baseButton.vue'

Vue.use(VueRouter) // 使用 vue-router

export default new VueRouter({
  mode: 'history', // 去掉路由中的＃
  routes: [
    {
      name: '首頁', // 元件呈現的名稱
      path: '/home', // 對應的路徑
      component: Home, // 對應的元件

    },
    /* {
      name: '404', // 元件呈現的名稱
      path: '*', // 對應的路徑
      redirect: '/home', // 對應的元件

    }, */
    {
      name: 'Login', // 元件呈現的名稱
      path: '/Login', // 對應的路徑
      component: Login, // 對應的元件
    },
    {
      name: 'tinymceEditor', // 元件呈現的名稱
      path: '/tinymceEditor', // 對應的路徑
      component: tinymceEditor, // 對應的元件
    },
    {
      name: 'elementUI', // 元件呈現的名稱
      path: '/elementUI', // 對應的路徑
      component: elementUI, // 對應的元件
    },
    {
      name: 'MemberManagement', // 元件呈現的名稱
      path: '/home/MemberManagement', // 對應的路徑
      component: MemberManagement, // 對應的元件
    },
    {
      name: 'MemberMemberLevels', // 元件呈現的名稱
      path: '/home/MemberLevels', // 對應的路徑
      component: MemberLevels, // 對應的元件
    },
    {
      name: 'levelUserList', // 元件呈現的名稱
      path: '/home/MemberLevels/level-user-list', // 對應的路徑
      component: levelUserList, // 對應的元件
    },
  ],
})

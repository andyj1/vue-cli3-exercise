// vue.config.js
module.exports = {
  publicPath: '/',
  outputDir: 'dist',
  lintOnSave: true,
  runtimeCompiler: 'false',
  chainWebpack: () => {},
  configureWebpack: () => {},
  devServer: {
    hot: true, // 热加载
    host: 'moa.gakkixmasami.com', // ip地址
    port: 8085, // 端口
    open: true, // 自动打开浏览器

  },

}

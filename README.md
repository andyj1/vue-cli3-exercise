# my-vue-cli3

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Run your tests
```
yarn run test
```

### Lints and fixes files
```
yarn run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### axios不能通过Vue.use(axios)的方式引入, 如果想在所有组件中直接使用axios
import axios from 'axios';
Vue.prototype.$http = axios;

### axios get使用方式
axios
.get('')
.then(response => {    
    console.log(response.data)
    return this.message = response.data
})
.catch(function (error) { // 请求失败处理
    console.log(error);
});

### 修改hosts
sudo vi /private/etc/hosts
i => 編輯
esc > :wq => 保存退出

### Eslint 修復
eslint --ext .js,.vue src --fix
